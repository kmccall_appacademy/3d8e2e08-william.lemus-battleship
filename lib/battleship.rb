require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player
  def initialize(player, board)
    @player = player
    @board = board
  end

  def play
    play_turn until game_over?
    puts "Game Over! All Ships Sank!"
  end

  def attack(pos)
    @board.place(pos, :x)
    display
  end

  def display
    # TODO: only display what has been destroyed.
    @board.grid.each { |r| puts r.to_s }
    # for bonus this count will be more complicated
    puts "#{count} remaining ship(s)"
  end

  def play_turn
    pos = player.get_play
    attack(pos.map(&:to_i))
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end
end

board = Board.new

3.times { board.place_random_ship }
game = BattleshipGame.new(HumanPlayer.new, board)

game.play
