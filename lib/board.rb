class Board
  attr_reader :grid, :default_grid
  def initialize(grid = [])
    if grid.empty?
      @grid = Board.default_grid
    else
      @grid = grid
    end
  end

  def place(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def [](pos)
    @grid[pos[0]][pos[1]]
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    @grid.flatten.count { |el| el == :s }
  end

  def full?
    count == @grid.flatten.length
  end

  def won?
    count.zero?
  end

  def empty?(pos = [])
    return count.zero? if pos.empty?
    @grid[pos[0]][pos[1]].nil?
  end

  def place_random_ship
    raise 'Full Board' if full?
    empty_positions = empty_pos
    place(empty_positions[rand(0...empty_positions.length)], :s )
  end

  def empty_pos
    empty = []
    @grid.each_index do |idx1|
      @grid[idx1].each_index do |idx2|
        empty << [idx1, idx2] if empty?([idx1, idx2])
      end
    end
    empty
  end

end
